//
//  ViewController.m
//  ToggleSegmentedControlTest
//
//  Created by Clark Barry on 8/27/12.
//  Copyright (c) 2012 CSHaus. All rights reserved.
//

#import "ViewController.h"
#import "ToggleSegmentedControl.h"
@interface ViewController ()
@end

@implementation ViewController

-(void)loadView{
    [super loadView];
    ToggleSegmentedControl *control = [[ToggleSegmentedControl alloc]initWithItems:@[@"A",@"B",@"C"]];
    control.frame = CGRectMake(100, 100, 200, 100);
    [self.view addSubview:control];
}



@end
